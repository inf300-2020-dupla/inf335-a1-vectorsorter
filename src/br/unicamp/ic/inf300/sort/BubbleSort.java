package br.unicamp.ic.inf300.sort;


public class BubbleSort extends Sorter {

	@Override
	public void sort(int[] vector) {
		boolean switched = true;
		for (int i = 0; i < vector.length - 1; i++) {
			switched = false;
			for (int j = 0; j < vector.length - i - 1; j++) {
				if (vector[j] > vector[j + 1]) {
					switched = swap(vector, j);
				}
			}
			if (switched == false) {
				break;
			}
		}
	}

	/**
	 * Método swap que foi extraído
	 * @param vector
	 * @param j
	 * @return
	 */
	private boolean swap(int[] vector, int j) {
		boolean switched;
		int aux;
		aux = vector[j];
		vector[j] = vector[j + 1];
		vector[j + 1] = aux;
		switched = true;
		return switched;
	}
}
