package br.unicamp.ic.inf300.sort;

/**
 * Superclasse que possuí o método sort
 * @author guilherme
 *
 */
public abstract class Sorter {

	public Sorter() {
		super();
	}

	public abstract void sort(int[] vector);

}